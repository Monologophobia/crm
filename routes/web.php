<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([], function () {

    Voyager::routes();

    Route::get('crm', ['uses' => 'CRMController@contacts', 'as' => 'crm']);

    Route::get('crm/contacts', ['uses' => 'CRMController@contacts', 'as' => 'contacts']);
    Route::post('crm/contacts', ['uses' => 'CRMController@filterContacts', 'as' => 'filter_contacts']);
    Route::get('crm/contact/new', ['uses' => 'CRMController@newContact', 'as' => 'new_contact']);
    Route::post('crm/contact/save', ['uses' => 'CRMController@saveContact']);
    Route::get('crm/contact/{id}', ['uses' => 'CRMController@contact', 'as' => 'contact']);
    Route::get('crm/contact', ['uses' => 'CRMController@contacts', 'as' => 'contact']);

    Route::post('crm/contacts/{id}/thread', ['uses' => 'CRMController@getThread', 'as' => 'get_thread']);
    Route::post('crm/contacts/{id}/send-email', ['uses' => 'CRMController@sendEmail', 'as' => 'send_email']);

    //Route::get('crm/emails', ['uses' => 'CRMController@emails', 'as' => 'emails']);
    //Route::get('crm/email/{id}', ['uses' => 'CRMController@email', 'as' => 'email']);

});