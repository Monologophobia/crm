<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class getEmailSettings extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crm:get_settings';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Gets Email settings for listening to inbox and sent mail through IMAP';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {

        try {
            echo json_encode([
                'host'     => setting('email.host'),
                'port'     => setting('email.port'),
                'username' => setting('email.username'),
                'password' => setting('email.password'),
                'db'       => database_path() . '/database.sqlite',
                'sent_messages_folder' => setting('email.sent_folder')
            ]);
        }
        catch (\Exception $e) {
            echo $e->getMessage();
        }

    }

}
