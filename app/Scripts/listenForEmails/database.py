import sqlite3
import datetime

class database:

    def __init__(self, db_path, logging):

        # sqlite3 connect automatically creates the file if it doesn't exist
        self.connection = sqlite3.connect(db_path, check_same_thread = False)
        self.logging = logging

    def find_contact(self, email):
        """ finds a contact from their email address """
        cursor = self.connection.cursor()
        cursor.execute("SELECT id FROM contacts WHERE email = ?;", (email, ))
        return cursor.fetchone()

    def create_contact(self, name, email):
        """ creates a new contact from a received email with new status and today as action date """
        try:
            status = 1
            action_date = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            query = "INSERT INTO contacts (name, email, status, action_date, created_at, updated_at) VALUES (?, ?, ?, ?, ?, ?)"
            cursor = self.connection.cursor()
            cursor.execute(query, (name, email, status, action_date, action_date, action_date, ))
            self.connection.commit()
            return True
        except Exception as e:
            self.logging.exception("database")
            return False

    def get_contact(self, email, name):
        contact = self.find_contact(email)
        if not contact:
            self.create_contact(name, email)
            contact = self.find_contact(email)
        return contact

    def contact_is_now_pending(self, contact_id):
        try:
            cursor = self.connection.cursor()
            cursor.execute("UPDATE contacts SET status = 2 WHERE id = ?", (contact_id, ))
            self.connection.commit()
            return True
        except Exception as e:
            self.logging.exception("database")
            return False

    def check_handled_emails(self, message_id_array):
        cursor = self.connection.cursor()
        placeholders = ', ' . join("?" for unused in message_id_array)
        query= 'SELECT message_id FROM messages WHERE message_id IN (%s)' % placeholders
        cursor.execute(query, message_id_array)
        results = cursor.fetchall()
        results = [x for xs in results for x in xs]
        unhandled = []
        for message_id in message_id_array:
            if message_id not in results:
                unhandled.append(message_id)
        return unhandled
        
    def record_email(self, message_id, from_id, to_id, subject, description, sent_at):
        try:
            query = "INSERT INTO messages (message_id, from_id, to_id, subject, description, sent_at) VALUES (?, ?, ?, ?, ?, ?)"
            cursor = self.connection.cursor()
            cursor.execute(query, (message_id, from_id, to_id, subject, description, sent_at, ))
            self.connection.commit()
            self.update_last_received_or_contacted(from_id, to_id, sent_at)
            return True
        except Exception as e:
            self.logging.exception("database")
            return False

    def update_last_received_or_contacted(self, from_id, to_id, sent_at):

        cursor = self.connection.cursor()

        # get last contacted or received date
        if from_id:
            query = "SELECT last_received FROM contacts WHERE id = ?"
            contact_id = from_id
        elif to_id:
            query = "SELECT last_contacted FROM contacts WHERE id = ?"
            contact_id = to_id
        else:
            return False

        cursor.execute(query, (contact_id, ))
        result = cursor.fetchone()

        # update contact status if necessary
        if to_id:
            cursor.execute("SELECT status FROM contacts WHERE id = ?", (contact_id, ))
            status = cursor.fetchone()
            if status[0] == 1:
                self.contact_is_now_pending(contact_id)

        # if the new message has a date greater than the last received/contacted update it
        if result[0] is None or (sent_at > result[0]):
            if from_id:
                query = "UPDATE contacts SET last_received = ? WHERE id = ?"
            elif to_id:
                query = "UPDATE contacts SET last_contacted = ? WHERE id = ?"
            cursor.execute(query, (sent_at, contact_id, ))
            self.connection.commit()

    def update_status(self, to_id):
        # if status is new and we've contacted them
        # update their status to contacted
        query = "SELECT status FROM contacts WHERE id = ?"
        contact_id = to_id

    def shutdown(self):
        self.connection.close()