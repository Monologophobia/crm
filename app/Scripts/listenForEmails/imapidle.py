import threading
import email as emailutils
from imapclient import IMAPClient
from datetime import datetime, timedelta

class Idler(threading.Thread):
    """ creates a threaded object and listens to mail in the specified mailbox """

    def __init__(self, thread_id, mailbox, queue, args, database, logging):

        threading.Thread.__init__(self)
        self.thread_id = thread_id

        self.args = args
        self.mailbox = mailbox
        self.queue = queue
        self.database = database
        self.logging = logging

        self.shutdown = threading.Event()

        self.server = IMAPClient(self.args['host'], ssl=True)
        self.server.login(self.args['username'], self.args['password'])
        self.server.select_folder(self.mailbox, readonly=True)

    def run(self):

        self.logging.info("Listening on " + self.mailbox)

        # while we're not in a shutdown state, listen and add responses to queue
        while not self.shutdown.is_set():

            self.server.idle()

            # timeout after 5 minutes (forces reconnect via if condition)
            push_received = self.server.idle_check(5*60)

            if push_received:
                self.server.idle_done()
                # get all unread emails
                # this doesn't work for sent mail. Instead, get all emails from past hour
                # although imap doesn't actually do time, just date... 
                # unseen_emails = self.server.search('UNSEEN')
                an_hour_ago = datetime.now() - timedelta(hours = 1)
                unseen_emails = self.server.search([u'SINCE', an_hour_ago.strftime("%d-%b-%Y")])
                # check if these emails have been handled already
                unhandled_emails = self.database.check_handled_emails(unseen_emails)
                # retrieve the unhandled email details
                emails = self.server.fetch(unhandled_emails, ['BODY[TEXT]', 'ENVELOPE'])
                # and add them to the queue
                for message_id, data in emails.items():
                    try:
                        envelope = data[b'ENVELOPE']
                        email = {
                            'message_id': message_id,
                            'subject': envelope.subject.decode(),
                            'text': emailutils.message_from_string(str(data[b'BODY[TEXT]'])),
                            'sent_at': envelope.date
                        }
                        if envelope.from_:
                            email['from_'] = {'name': False, 'email': False}
                            if envelope.from_[0].name:
                                email['from_']['name'] = envelope.from_[0].name.decode()
                            if envelope.from_[0].mailbox and envelope.from_[0].host:
                                email['from_']['email'] = envelope.from_[0].mailbox.decode() + "@" + envelope.from_[0].host.decode()
                        if envelope.to:
                            email['to'] = {'name': False, 'email': False}
                            if envelope.to[0].name:
                                email['to']['name'] = envelope.to[0].name.decode()
                            if envelope.to[0].mailbox and envelope.to[0].host:
                                email['to']['email'] = envelope.to[0].mailbox.decode() + "@" + envelope.to[0].host.decode()
                        self.queue.put(email)
                    except Exception as e:
                        self.logging.exception("idler")
            else:
                # do an occasional reconnect
                self.server.idle_done()
                self.server.noop()

            continue

        # safe shutdown
        self.server.idle_done()
        self.server.logout()