import os
import sys
import time
import json
import queue
import asyncio
import logging
import imapidle
import argparse
import threading
import subprocess
import logging.handlers
from multiprocessing import Process
from database import database as db

def main(arguments):
    """ Start the program """

    # setup work queues
    incoming_mail = queue.Queue()
    outgoing_mail = queue.Queue()
    logging.info("Queues set up")

    # setup database
    database = db(arguments['db'], logging)
    logging.info('DB set up')

    # setup inbox and outbox threads
    logging.info("Connecting to server on " + arguments['host'])
    try:
        inbox = Process(target = start_listener, args=("inbox", incoming_mail, arguments, database, logging))
        inbox.daemon = True
        inbox.start()
        outbox = Process(target = start_listener, args=("outbox", outgoing_mail, arguments, database, logging))
        outbox.daemon = True
        outbox.start()

        # wait here for inbox listener to end (or some crash to destroy everything)
        inbox.join()

    except Exception as e:
        logging.exception("main")
        sys.exit()

def log_setup():
    """ setup the logging interface """
    path = "logs/" + time.strftime("%Y-%m-%d") + ".log"
    handler = logging.handlers.TimedRotatingFileHandler(path, when = "d", interval = 1, backupCount = 7)
    formatter = logging.Formatter(
        '%(asctime)s [%(process)d]: %(message)s',
        '%b %d %H:%M:%S')
    formatter.converter = time.gmtime
    handler.setFormatter(formatter)
    logger = logging.getLogger()
    logger.addHandler(handler)
    logger.setLevel(logging.INFO)

def start_listener(thread_name, threaded_queue, arguments, database, logging):
    if thread_name == "inbox":
        # INBOX is a specific immutable IMAP folder
        mailbox = "INBOX"
    if thread_name == "outbox":
        # outbox mailname has to be user specified as it is changeable
        mailbox = arguments["sent_messages_folder"]
    idler = imapidle.Idler(thread_name, mailbox, threaded_queue, arguments, database, logging)
    idler.start()

    while True:

        try:
            mail = threaded_queue.get(True)
        except:
            mail = False

        if mail:

            logging.info('Processing ' + thread_name + ' message ID ' + str(mail['message_id']))

            try:

                from_to = 'from_' if thread_name == "inbox" else 'to'
                contact = database.get_contact(mail[from_to]['email'], mail[from_to]['name'])

                from_id = contact[0] if thread_name == "inbox" else False
                to_id = False if thread_name == "inbox" else contact[0]
                database.record_email(mail['message_id'], from_id, to_id, mail['subject'], False, mail['sent_at'].strftime("%Y-%m-%d %H:%M:%S"))

            except Exception as e:
                logging.exception("main")

        time.sleep(1)

if __name__ == '__main__':

    # when running from /usr/bin/python3 /whatever/this/is/main.py make sure the correct cwd is used
    os.chdir(sys.path[0])

    # get args from PHP application
    proc = subprocess.Popen("php ../../../artisan crm:get_settings", shell=True, stdout=subprocess.PIPE)
    args = proc.stdout.read()
    args = args.decode('utf8').replace("'", '"')

    log_setup()
    main(json.loads(args))