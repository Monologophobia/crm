<?php namespace App\Http\Controllers;

use Config;

use App\Contact;
use App\Mail\sendMessage;

use Fetch\Message;
use Fetch\Server as MailServer;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Routing\Controller as BaseController;

class CRMController extends BaseController {

    private $mailserver;
    
    public function __construct() {
        $this->middleware('auth');
        $this->mailserver = new MailServer(setting('email.host'), setting('email.port'));
        $this->mailserver->setAuthentication(setting('email.username'), setting('email.password'));
    }

    public function contacts(Request $request) {
        $limit = 30;
        $contacts = Contact::paginate($limit);
        $contact = new Contact;
        return view('crm.contacts', ['contacts' => $contacts, 'contact' => $contact]);
    }

    public function filterContacts(Request $request) {

        $limit = 30;
        $contacts = Contact::paginate($limit);

        $status = 0;
        $action_date = false;
        $last_received = false;
        $search = false;

        if ($request->input('status')) {
            $status = intval($request->input('status'));
            if ($status) {
                $contacts = $contacts->where('status', $status);
            }
        }
        if ($request->input('action_date')) {
            $action_date = date('Y-m-d H:i:s', strtotime($request->input('action_date')));
            $contacts = $contacts->where('action_date', '>=', $action_date);
        }
        if ($request->input('last_received')) {
            $last_received = date('Y-m-d H:i:s', strtotime($request->input('last_received')));
            $contacts = $contacts->where('last_received', '>=', $last_received);
        }
        if ($request->input('search')) {
            $search = $request->input('search');
            $contacts = $contacts->where('name', 'LIKE', "%$search%");
        }

        $contact = new Contact;
        return view('crm.contacts', ['contacts' => $contacts, 'contact' => $contact, 'status' => $status, 'action_date' => $action_date, 'last_received' => $last_received, 'search' => $search]);
    }

    public function contact(Request $request, $id = false) {
        try {
            $contact = Contact::find(intval($id));
            return view('crm.contacts.edit', ['contact' => $contact]);
        }
        catch (\Exception $e) {
            $this->displayMessage($request, $e->getMessage(), false);
            return redirect()->to('contacts');
        }
    }

    public function newContact(Request $request) {
        return view('crm.contacts.new');
    }

    public function saveContact(Request $request) {
        $post = $request->all();
        print_r($post);
        if (count($post) > 0) {
            try {

                $contact = new Contact;
                if (isset($post['id'])) {
                    $contact = Contact::findOrFail(intval($post['id']));
                }

                if (isset($post['name'])) {
                    $contact->name        = filter_var($post['name'], FILTER_SANITIZE_STRING);
                }
                if (isset($post['email'])) {
                    $contact->email       = filter_var($post['email'], FILTER_SANITIZE_EMAIL);
                }
                if (isset($post['status'])) {
                    $contact->status      = intval($post['status']);
                }
                if (isset($post['action_date'])) {
                    $contact->action_date = date('Y-m-d', strtotime($post['action_date']));
                }
                if (isset($post['notes'])) {
                    $contact->notes       = filter_var($post['notes'], FILTER_SANITIZE_STRING);
                }

                $contact->save();

                $this->displayMessage($request, 'Saved Contact');
                return redirect()->to('/crm/contacts');

            }
            catch (\Exception $e) {
                print_r($e); die();
                $this->displayMessage($request, $e->getMessage(), false);
                return redirect()->to('/crm/contacts');
            }
        }
    }

    public function emails(Request $request) {
        try {

            $messages = $this->getMessages();

            return view('crm.emails.list', ['emails' => $messages]);

        }
        catch (\Exception $e) {
            $this->displayMessage($request, $e->getMessage(), false);
            return view('crm.emails.list');
        }
    }

    public function email(Request $request, $uid) {
        try {
            $email = $this->mailserver->getMessageByUid($uid);
            return view('crm.emails.email', ['email' => $email]);
        }
        catch (\Exception $e) {
            $this->displayMessage($request, $e->getMessage(), false);
            return redirect()->to('emails');
        }
    }

    public function getMessages() {
        $messages = $this->mailserver->getOrderedMessages(SORTDATE, 1, 30);
        $return   = [];
        // process messages
        foreach ($messages as $message) {
            try {

                $sender = $message->getAddresses('sender');

                // update or create contact
                $contact = Contact::where('email', $sender["address"])->first();
                if (!$contact) $contact = new Contact;
                if (!$contact->name) $contact->name  = $sender["name"];
                if (!$contact->email) $contact->email = $sender["address"];
                if (!$contact->last_received) {
                    $contact->last_received = date('Y-m-d H:i:s', $message->getDate());
                }
                else if (strtotime($contact->last_received) < $message->getDate()) {
                    $contact->last_received = date('Y-m-d H:i:s', $message->getDate());
                }
                $contact->save();

                $object = new \stdclass;
                $object->contact_id = $contact->id;
                $object->message = $message;

                $return[] = $object;

            }
            catch (\Exception $e) {
                $object = new \stdclass;
                $object->message = $message;
                $return[] = $object;
            }
        }
        return $return;
    }

    public function getThread(Request $request, $id) {
        try {
            $post = $request->all();
            $message_ids = json_decode($post['ids']);
            $contact  = Contact::findOrFail(intval($id));
            $messages = [];
            foreach ($message_ids as $message_id) {
                $message = $this->mailserver->getMessageByUid($message_id);
                if ($message instanceof \Fetch\Message) $messages[] = $message;
            }
            return view('crm.emails.thread', ['messages' => $messages, 'contact' => $contact, 'subject' => $post['subject']]);
        }
        catch (\Exception $e) {
            $this->displayMessage($request, $e->getMessage(), false);
            return redirect()->to('/crm/contacts');
        }
    }

    public function sendEmail(Request $request, $id) {
        try {
            $post = $request->all();
            $subject = filter_var($post['subject'], FILTER_SANITIZE_STRING);
            $html = $post['body'];
            $plain = \Html2Text\Html2Text::convert($html);
            $contact = Contact::findOrFail(intval($id));
            Config::set('mail.driver', 'smtp');
            Config::set('mail.port', setting('email.outgoing_port'));
            Config::set('mail.host', setting('email.outgoing_host'));
            Config::set('mail.username', setting('email.username'));
            Config::set('mail.password', setting('email.password'));
            Config::set('mail.encryption', 'ssl');
            Mail::to($contact->email, $contact->name)->send(new sendMessage($subject, $html, $plain));
            // if this is a new contact mark them as contacted
            if ($contact->isNew()) {
                $contact->status = 2;
                $contact->save();
            }
            $this->displayMessage($request, 'Email Sent');
            return redirect()->to('/crm/contact/' . $id);
        }
        catch (\Exception $e) {
            $this->displayMessage($request, $e->getMessage(), false);
            return redirect()->to('/crm/contact/' . $id);
        }
    }

    private function displayMessage(Request $request, $message, $success = true) {
        $request->session()->flash('message', $message);
        $type = ($success ? 'success' : 'error');
        $request->session()->flash('alert-type', $type);
    }

}
