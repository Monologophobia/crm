<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class sendMessage extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject, $html, $plain)
    {
        $this->subject = utf8_encode($subject);
        $this->html = $html;
        $this->plain = $plain;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(setting('email.username'), setting('email.name'))
                    ->subject($this->subject)
                    ->view('emails.send')
                    ->text('emails.send_plain')
                    ->with([
                        'subject' => $this->subject,
                        'html' => $this->html,
                        'plain' => $this->plain
                    ]);
    }

}
