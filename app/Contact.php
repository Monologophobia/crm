<?php namespace App;

use DB;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model {

    protected $table = 'contacts';

    public $timestamps = true;
    protected $dateFormat = 'Y-m-d H:i:s';

    public function getStatusOptions() {
        return [
            1 => 'New',
            2 => 'Emailed / Quoted',
            3 => 'Follow Up',
            4 => 'Second Follow Up',
            5 => 'Third Follow Up',
            6 => 'Formal Quote',
            7 => 'Booked',
            8 => 'Lost'
        ];
    }

    public function getMessages() {
        $from = DB::table('messages')->where('from_id', $this->id)->get();
        $to   = DB::table('messages')->where('to_id', $this->id)->get();
        $merged = $from->merge($to);
        $messages = $merged->all();
        usort($messages, function($a, $b) {
            return $a->sent_at <=> $b->sent_at;
        });
        return $messages;
    }

    public function getMessagesBySubject() {
        $messages = [];
        foreach ($this->getMessages() as $message) {
            $subject = $message->subject;
            $subject = str_replace("Re: ", "", $subject);
            $subject = html_entity_decode($subject);
            if (!isset($messages[$subject])) $messages[$subject] = [];
            $messages[$subject][] = $message;
        }
        return $messages;
    }

    /**
     * Get all new contacts
     * @return Scoped Collection
     */
    public function scopeNew($query) {
        return $query->where('status', 1);
    }

    public function scopePending($query) {
        return $query->where('status', 2);
    }

    public function scopeAfterActionDate($query) {
        return $query->where('action_date', '<=', date('Y-m-d H:i:s'));
    }

    /**
     * Is this marked as status new
     * @return boolean
     */
    public function isNew() {
        if ($this->status == 1) return true;
        return false;
    }

}
