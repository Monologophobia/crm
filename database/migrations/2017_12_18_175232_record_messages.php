<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RecordMessages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function($table) {
            $table->increments('id');
            $table->integer('message_id')->unique()->index();
            $table->integer('from_id')->nullable();
            $table->integer('to_id')->nullable();
            $table->text('subject')->nullable();
            $table->text('description')->nullable();
            $table->dateTime('sent_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
