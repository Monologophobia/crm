@extends('voyager::master')

@section('css')
<style>
.person-details {
    padding: 0.25rem 1.5rem 1.25rem 1.5rem;
}
</style>
@stop

@section('content')
<br />
<div class="page-content container-fluid">

    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-bordered person-details">
                <h1>{{ $contact->name }}</h1>
                <h2><a href="mailto:{{ $contact->email }}" title="{{ $contact->email }}">{{ $contact->email }}</a></h2><br />
                <p>Last Contacted: <strong>@if ($contact->last_contacted){{ date('D j M Y \a\t H:i', strtotime($contact->last_contacted)) }}@endif</strong></p>
                <p>Last Heard From: <strong>@if ($contact->last_received){{ date('D j M Y \a\t H:i', strtotime($contact->last_received)) }}@endif</strong></p>
                <p>Action Date: <strong>@if ($contact->action_date){{ date('D j M Y \a\t H:i', strtotime($contact->action_date)) }}@endif</strong></p>
            </div>
            <div class="panel panel-bordered person-details">
                <h3>Actions</h3>
                <form method="POST" action="/crm/contact/save" class="form">
                    {{ csrf_field() }}
                    <input name="id" value="{{ $contact->id }}" type="hidden" />
                    <div class="form-group">
                        <label>Status</label>
                        <select name="status" class="form-control">
                            @foreach ($contact->getStatusOptions() as $key => $value)
                            <option value="{{ $key }}" @if ($contact->status == $key) selected @endif>{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Action Date</label>
                        <input type="date" name="action_date" value="{{ $contact->action_date }}" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label>Notes</label>
                        <textarea name="notes" class="form-control">{{ $contact->notes }}</textarea>
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary" value="Update Contact" />
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-bordered">
                <div class="panel-body" style="padding:30px;">
                    <h3>Contact History</h3>

                    @foreach ($contact->getMessagesBySubject() as $thread => $messages)

                        @php
                        $message_ids = [];
                        foreach ($messages as $message) {
                            $message_ids[] = $message->message_id;
                        }
                        @endphp

                        <div class="panel panel-default">
                            <div class="panel-heading" style="padding: 0.75rem 1rem">
                                <strong><a href="#" data-ids='{{ json_encode($message_ids) }}' data-subject="{{ $thread }}" class="load-messages">{{ $thread }}</a></strong>
                            </div>
                            <div class="panel-body" style="padding-top: 15px;">
                        @foreach ($messages as $message)
                            @if ($message->from_id)
                                <div class="icon voyager-forward" style="display: inline-block;"></div> {{ date('D j M Y \a\t H:i', strtotime($message->sent_at)) }}<br />
                            @elseif ($message->to_id)
                                <div class="icon voyager-download" style="display: inline-block;"></div> {{ date('D j M Y \a\t H:i', strtotime($message->sent_at)) }}<br />
                            @endif
                        @endforeach
                            </div>
                        </div>
                    @endforeach
                    
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="panel panel-bordered">
                <div class="panel-body" style="padding:30px;" id="message">
                    @include('crm.emails.new', ['contact' => $contact])
                </div>
            </div>
        </div>
    </div>

</div>

<script>
get_message = document.getElementsByClassName('load-messages');
for (var i = 0; i < get_message.length; i++) {
    get_message[i].onclick = loadMessage;
}
function loadMessage(e) {
    e.preventDefault;
    var message_area = document.getElementById('message');
    message.innerHTML = '<div id="voyager-loader" style="height: 200px; position: relative;">' +
                        '<img src="http://crm.monoquill.co.uk/vendor/tcg/voyager/assets/images/logo-icon.png" alt="Voyager Loader" style="top: 25%;">' +
                        '</div>';

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        'url':'/crm/contacts/{{ $contact->id }}/thread',
        'type': 'POST',
        'data': {'ids': JSON.stringify($(e.target).data('ids')), 'subject': $(e.target).data('subject')},
        'success': function(result) {
            message_area.innerHTML = result;
            var element = document.getElementById('editor');
            var editor = new Quill(element, {
                modules: { toolbar: true },
                theme: 'snow'
            });
            editor.on('text-change', function() {
                var element = document.querySelector('#send-email textarea[name="body"]');
                element.innerHTML = editor.root.innerHTML;
            });
        }
    });

}
</script>

@stop
