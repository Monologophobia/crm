@php
$contact = new \App\Contact;
@endphp

@extends('voyager::master')

@section('css')
<style>
.person-details {
    padding: 0.25rem 1.5rem 1.25rem 1.5rem;
}
</style>
@stop

@section('content')
<br />
<div class="page-content container-fluid">

    <form method="POST" action="/crm/contact/save" class="panel panel-bordered person-details">
        <h1>New Contact</h1><br />
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Name</label>
                    <input type="text" name="name" class="form-control" required />
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input type="email" name="email" class="form-control" required />
                </div>
                <div class="form-group">
                    <label>Status</label>
                    <select name="status" class="form-control">
                        @foreach ($contact->getStatusOptions() as $key => $value)
                        <option value="{{ $key }}" @if ($contact->status == $key) selected @endif>{{ $value }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Action Date</label>
                    <input type="date" name="action_date" value="{{ $contact->action_date }}" class="form-control" />
                </div>
                <div class="form-group">
                    <label>Notes</label>
                    <textarea name="notes" class="form-control"></textarea>
                </div>
            </div>
        </div>
        <input type="submit" value="Save Contact" class="btn btn-primary" />
    </form>

</div>
@stop
