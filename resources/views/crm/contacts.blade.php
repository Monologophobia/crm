@extends('voyager::master')

@section('page_title', "Contacts")

@section('css')
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-people"></i> Contacts
        <a href="{{ route('new_contact') }}" class="btn btn-success">
            <i class="voyager-plus"></i> Add Contact
        </a>
    </h1>
@stop

@section('content')
<div class="page-content container-fluid">
    
    <form method="POST" class="panel">
        {{ csrf_field() }}
        <fieldset>
            <legend>Filters</legend>
            <div class="row">
                <div class="col-md-2">
                    <label>Status</label>
                    <select name="status" class="form-control">
                        <option value="0">All</option>
                        @foreach ($contact->getStatusOptions() as $key => $value)
                        <option value="{{ $key }}" @if (isset($status) && $status == $key) selected @endif>{{ $value }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-md-2">
                    <label>Action Date After</label>
                    <input type="date" name="action_date" class="form-control" @if (isset($action_date)) value="{{ $action_date }}" @endif />
                </div>
                <div class="col-md-2">
                    <label>Last Received After</label>
                    <input type="date" name="last_received" class="form-control" @if (isset($last_received)) value="{{ $last_received }}" @endif  />
                </div>
                <div class="col-md-2">
                    <label>Name / Email</label>
                    <input type="text" name="search" class="form-control" @if (isset($search)) value="{{ $search }}" @endif  />
                </div>
                <div class="col-md-2">
                    <input type="submit" value="Filter" class="btn btn-primary" />
                </div>
            </div>
        </fieldset>
    </form>

    <table class="table">
        <thead>
            <tr>
                <th>Name</th>
                <th>Status</th>
                <th>Action By</th>
                <th>Last Contacted</th>
                <th>Last Received</td>
                <th>Created</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($contacts as $contact)
            <tr>
                <td><a href="/crm/contact/{{ $contact->id }}" title="{{ $contact->name }}">{{ $contact->name }}</a><br />{{ $contact->email }}</td>
                <td>{{ $contact->getStatusOptions()[$contact->status] }}</td>
                <td>@if ($contact->action_date){{ date('dS M Y H:i', strtotime($contact->action_date)) }}@endif</td>
                <td>@if ($contact->last_contacted){{ date('dS M Y H:i', strtotime($contact->last_contacted)) }}@endif</td>
                <td>@if ($contact->last_received){{ date('dS M Y H:i', strtotime($contact->last_received)) }}@endif</td>
                <td>{{ date('dS M Y H:i', strtotime($contact->created_at)) }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

</div>
@stop
