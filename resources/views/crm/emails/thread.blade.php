<h3>{{ $subject }}</h3>

@foreach ($messages as $message)
@php
$sender = $message->getAddresses('sender');
@endphp

<div class="panel panel-bordered panel-default">
    <div class="panel-heading" style="padding: 0.75rem 1rem;">
        On {{ date('D j M Y \a\t H:i', $message->getDate()) }} {{ $sender['name'] }}  <{{ $sender['address'] }}> wrote:
    </div>
    <div class="panel-body" style="padding-top: 20px">
        {!! $message->getMessageBody(true) !!}
    </div>
</div>
@endforeach

@include('crm.emails.new', ['contact' => $contact, 'subject' => $subject])