<h3>Send Email</h3>

<form method="POST" action="/crm/contacts/{{ $contact->id }}/send-email" class="form" id="send-email">
    {{ csrf_field() }}
    <div class="form-group">
        <input type="text" name="subject" placeholder="Subject" value="{!! isset($subject) ? "Re: " . $subject : "" !!}" style="width: 100%;" required />
    </div>
    <div class="form-group">
        <div id="editor" style="min-height: 300px;"></div>
        <textarea name="body" style="display: none;"></textarea>
    </div>
    <div class="form-group">
        <input type="submit" class="btn btn-primary" value="Send to {{ $contact->email }}" />
    </div>
</form>

<script src="//cdn.quilljs.com/1.3.4/quill.js"></script>
<script src="//cdn.quilljs.com/1.3.4/quill.min.js"></script>
<link href="//cdn.quilljs.com/1.3.4/quill.snow.css" rel="stylesheet">
<link href="//cdn.quilljs.com/1.3.4/quill.bubble.css" rel="stylesheet">
<script>
var element = document.getElementById('editor');
var editor = new Quill(element, {
    modules: { toolbar: true },
    theme: 'snow'
});
editor.on('text-change', function() {
    var element = document.querySelector('#send-email textarea[name="body"]');
    element.innerHTML = editor.root.innerHTML;
});
</script>