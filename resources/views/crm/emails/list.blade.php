@extends('voyager::master')

@section('css')
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-chat"></i> Emails
        <a href="{{ route('contact') }}" class="btn btn-success">
            <i class="voyager-plus"></i> New Email
        </a>
    </h1>
@stop

@section('content')
<div class="page-content container-fluid">
<div class="panel panel-bordered">
    <div class="panel-body">
        <table class="table">
            <thead>
                <tr>
                    <th>From</th>
                    <th>Subject</th>
                    <th>Sent</th>
                </tr>
            </thead>
            <tbody>
            @if (isset($emails))
                @foreach ($emails as $email)
                    @php
                        $sender = $email->message->getAddresses('sender');
                    @endphp
                <tr>
                    <td><a href="/crm/contact/@if (isset($email->contact_id)){{ $email->contact_id }}@endif" title="{{ $sender["name"] }}">{{ $sender["name"] }}</a><br /><small>{{ $sender["address"] }}</small></td>
                    <td>@if (!$email->message->checkFlag('seen'))<strong>@endif<a href="/crm/email/{{ $email->message->getUid() }}" title="{{ $email->message->getSubject() }}">{{ $email->message->getSubject() }}</a>@if (!$email->message->checkFlag('seen'))</strong>@endif</td>
                    <td>{{ date('dS M Y H:i', $email->message->getDate()) }}</td>
                </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>
</div>
</div>
@stop
