@extends('voyager::master')

@section('css')
@stop

@section('content')
<div class="page-content container-fluid">
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-bordered">
            <div class="panel-heading">
                <p class="panel-title" style="color:#777">{{ $email->getSubject() }}</p>
            </div>

            <div class="panel-body" style="padding:30px;">
                <iframe sandbox id="email"></iframe>
                <script>
                document.getElementById('email').contentDocument.write("{{ $email->getMessageBody(true) }}");
                </script>
            </div>
        </div>
    </div>
</div>
</div>
@stop
