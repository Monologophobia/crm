@php
$new = \App\Contact::new()->get()->count();
$pending = \App\Contact::pending()->get()->count();
$actionable = \App\Contact::afterActionDate()->get()->count();
@endphp

<div class="clearfix container-fluid row">

    <div class="col-md-4">
        <div class="panel widget center bgimage" style="margin-bottom:0; overflow:hidden; background-image:url('/vendor/tcg/voyager/assets/images/widget-backgrounds/01.jpg');">
            <div class="dimmer"></div>
            <div class="panel-content">
                <i class='voyager-group'></i>
                <h4>{{ $new }} New Contacts</h4>
                <p>You have {{ $new }} new users to investigate.</p>
                <a href="/crm/contacts" class="btn btn-primary">View all contacts</a>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="panel widget center bgimage" style="margin-bottom:0; overflow:hidden; background-image:url('/vendor/tcg/voyager/assets/images/widget-backgrounds/02.jpg');">
            <div class="dimmer"></div>
            <div class="panel-content">
                <i class='voyager-group'></i>
                <h4>{{ $pending }} Pending Contacts</h4>
                <p>You have {{ $pending }} contacts waiting for a reply.</p>
                <a href="/crm/contacts" class="btn btn-primary">View all contacts</a>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="panel widget center bgimage" style="margin-bottom:0; overflow:hidden; background-image:url('/vendor/tcg/voyager/assets/images/widget-backgrounds/03.jpg');">
            <div class="dimmer"></div>
            <div class="panel-content">
                <i class='voyager-group'></i>
                <h4>{{ $actionable }} Actions past due</h4>
                <p>There are {{ $new }} contacts with actions that are past their due date.</p>
                <a href="/crm/contacts" class="btn btn-primary">View all contacts</a>
            </div>
        </div>
    </div>

</div>
